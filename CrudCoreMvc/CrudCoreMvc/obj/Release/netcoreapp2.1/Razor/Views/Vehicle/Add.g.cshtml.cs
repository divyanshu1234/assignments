#pragma checksum "C:\Users\divyanshu.bansal\source\repos\CrudCoreMvc\CrudCoreMvc\Views\Vehicle\Add.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "1eba4328c51d737ac9919a848cc47662dcff0f19"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Vehicle_Add), @"mvc.1.0.view", @"/Views/Vehicle/Add.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Vehicle/Add.cshtml", typeof(AspNetCore.Views_Vehicle_Add))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\divyanshu.bansal\source\repos\CrudCoreMvc\CrudCoreMvc\Views\_ViewImports.cshtml"
using CrudCoreMvc;

#line default
#line hidden
#line 2 "C:\Users\divyanshu.bansal\source\repos\CrudCoreMvc\CrudCoreMvc\Views\_ViewImports.cshtml"
using CrudCoreMvc.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1eba4328c51d737ac9919a848cc47662dcff0f19", @"/Views/Vehicle/Add.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5119371945ebd20e85b148d4dc1d5d74f82b0186", @"/Views/_ViewImports.cshtml")]
    public class Views_Vehicle_Add : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<CrudCoreMvc.Models.Vehicle>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "C:\Users\divyanshu.bansal\source\repos\CrudCoreMvc\CrudCoreMvc\Views\Vehicle\Add.cshtml"
  
    ViewData["Title"] = "Add";

#line default
#line hidden
            BeginContext(74, 16, true);
            WriteLiteral("<h1>Add</h1>\r\n\r\n");
            EndContext();
#line 7 "C:\Users\divyanshu.bansal\source\repos\CrudCoreMvc\CrudCoreMvc\Views\Vehicle\Add.cshtml"
 using (Html.BeginForm("Add", "Vehicle", FormMethod.Post, new { enctype = "multipart/form-data" }))
{

#line default
#line hidden
            BeginContext(194, 38, true);
            WriteLiteral("    <div class=\"form-group\">\r\n        ");
            EndContext();
            BeginContext(233, 31, false);
#line 10 "C:\Users\divyanshu.bansal\source\repos\CrudCoreMvc\CrudCoreMvc\Views\Vehicle\Add.cshtml"
   Write(Html.LabelFor(m => m.VehicleId));

#line default
#line hidden
            EndContext();
            BeginContext(264, 10, true);
            WriteLiteral("\r\n        ");
            EndContext();
            BeginContext(275, 66, false);
#line 11 "C:\Users\divyanshu.bansal\source\repos\CrudCoreMvc\CrudCoreMvc\Views\Vehicle\Add.cshtml"
   Write(Html.TextBoxFor(m => m.VehicleId, new { @class = "form-control" }));

#line default
#line hidden
            EndContext();
            BeginContext(341, 14, true);
            WriteLiteral("\r\n    </div>\r\n");
            EndContext();
            BeginContext(357, 38, true);
            WriteLiteral("    <div class=\"form-group\">\r\n        ");
            EndContext();
            BeginContext(396, 26, false);
#line 15 "C:\Users\divyanshu.bansal\source\repos\CrudCoreMvc\CrudCoreMvc\Views\Vehicle\Add.cshtml"
   Write(Html.LabelFor(m => m.Name));

#line default
#line hidden
            EndContext();
            BeginContext(422, 10, true);
            WriteLiteral("\r\n        ");
            EndContext();
            BeginContext(433, 61, false);
#line 16 "C:\Users\divyanshu.bansal\source\repos\CrudCoreMvc\CrudCoreMvc\Views\Vehicle\Add.cshtml"
   Write(Html.TextBoxFor(m => m.Name, new { @class = "form-control" }));

#line default
#line hidden
            EndContext();
            BeginContext(494, 14, true);
            WriteLiteral("\r\n    </div>\r\n");
            EndContext();
            BeginContext(510, 38, true);
            WriteLiteral("    <div class=\"form-group\">\r\n        ");
            EndContext();
            BeginContext(549, 27, false);
#line 20 "C:\Users\divyanshu.bansal\source\repos\CrudCoreMvc\CrudCoreMvc\Views\Vehicle\Add.cshtml"
   Write(Html.LabelFor(m => m.Color));

#line default
#line hidden
            EndContext();
            BeginContext(576, 10, true);
            WriteLiteral("\r\n        ");
            EndContext();
            BeginContext(587, 62, false);
#line 21 "C:\Users\divyanshu.bansal\source\repos\CrudCoreMvc\CrudCoreMvc\Views\Vehicle\Add.cshtml"
   Write(Html.TextBoxFor(m => m.Color, new { @class = "form-control" }));

#line default
#line hidden
            EndContext();
            BeginContext(649, 14, true);
            WriteLiteral("\r\n    </div>\r\n");
            EndContext();
            BeginContext(665, 38, true);
            WriteLiteral("    <div class=\"form-group\">\r\n        ");
            EndContext();
            BeginContext(704, 27, false);
#line 25 "C:\Users\divyanshu.bansal\source\repos\CrudCoreMvc\CrudCoreMvc\Views\Vehicle\Add.cshtml"
   Write(Html.LabelFor(m => m.Price));

#line default
#line hidden
            EndContext();
            BeginContext(731, 10, true);
            WriteLiteral("\r\n        ");
            EndContext();
            BeginContext(742, 62, false);
#line 26 "C:\Users\divyanshu.bansal\source\repos\CrudCoreMvc\CrudCoreMvc\Views\Vehicle\Add.cshtml"
   Write(Html.TextBoxFor(m => m.Price, new { @class = "form-control" }));

#line default
#line hidden
            EndContext();
            BeginContext(804, 14, true);
            WriteLiteral("\r\n    </div>\r\n");
            EndContext();
            BeginContext(820, 38, true);
            WriteLiteral("    <div class=\"form-group\">\r\n        ");
            EndContext();
            BeginContext(859, 30, false);
#line 30 "C:\Users\divyanshu.bansal\source\repos\CrudCoreMvc\CrudCoreMvc\Views\Vehicle\Add.cshtml"
   Write(Html.LabelFor(m => m.Capacity));

#line default
#line hidden
            EndContext();
            BeginContext(889, 10, true);
            WriteLiteral("\r\n        ");
            EndContext();
            BeginContext(900, 65, false);
#line 31 "C:\Users\divyanshu.bansal\source\repos\CrudCoreMvc\CrudCoreMvc\Views\Vehicle\Add.cshtml"
   Write(Html.TextBoxFor(m => m.Capacity, new { @class = "form-control" }));

#line default
#line hidden
            EndContext();
            BeginContext(965, 14, true);
            WriteLiteral("\r\n    </div>\r\n");
            EndContext();
            BeginContext(981, 41, true);
            WriteLiteral("    <input type=\"submit\" value=\"save\"/>\r\n");
            EndContext();
#line 35 "C:\Users\divyanshu.bansal\source\repos\CrudCoreMvc\CrudCoreMvc\Views\Vehicle\Add.cshtml"
}

#line default
#line hidden
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<CrudCoreMvc.Models.Vehicle> Html { get; private set; }
    }
}
#pragma warning restore 1591
