﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrudCoreMvc.Manager;
using CrudCoreMvc.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CrudCoreMvc.Controllers
{

   /* [Route("vehicle")]*/
    public class VehicleController : Controller
    {
        private VehicleManager context=new VehicleManager();

      /*  public VehicleController(IVehicleManager context)
        {
            this.context = context;
        }*/
       
       /* VehicleManager vehicleManager=new VehicleManager();
        
        private DataContext db = new DataContext();
        [Route("")]
        [Route("index")]
        [Route("~/")]*/
        public IActionResult Index()
        {
            try
            {
                List<Vehicle> list = context.Display();
                if(list==null)
                {
                    return BadRequest("no record found");
                }
                return View(context.Display());
            }
            catch(Exception ex)
            {
                return Content(ex.Message);
            }
            /*ViewBag.vehicles =vehicleManager.Get();
            return View();*/
        }

        [HttpGet]
       /* [Route("Add")]*/
        public ActionResult Add()
        {
            return View();
          /*  return View("Add", new Vehicle());*/
        }

        [HttpPost]
       /* [Route("Add")]*/
        public ActionResult Add(Vehicle vehicle)
        {
            try
            {
                if(vehicle==null)
                {
                    return BadRequest("no element entered");
                }
                Vehicle newVehicle = context.Create(vehicle);
                if(newVehicle==null)
                {
                    return StatusCode(500);
                }
                return View("Index", context.Display());
            }
            catch(Exception ex)
            {
                return Content(ex.Message);
            }
            /*vehicleManager.Create(vehicle);
               
                
           
                return RedirectToAction("Index");*/
        }

        [HttpGet]
        /*[Route("Delete/{id}")]*/
        public ActionResult Delete()
        {
            /*  vehicleManager.Delete(id);

              return RedirectToAction("Index");*/
            return View();
        }

        [HttpPost]
        public ActionResult Delete(int vehicleId)
        {
            try
            {
                Vehicle vehicle = context.Delete(vehicleId);
                if(vehicle==null)
                {
                    return BadRequest("Id not found");
                }
              //  context.Delete(id);
                return View("Index", context.Display());
            }
            catch(Exception ex)
            {
                return Content(ex.Message);
            }
        }

        [HttpGet]
        public ActionResult FindVehicle()
        {
            return View();
        }

        [HttpPost]
        public ActionResult FindVehicle(int vehicleId)
        {
            try
            {
                Vehicle vehicle = context.GetById(vehicleId);
                if(vehicle==null)
                {
                    return BadRequest("id not found");
                }
                return View("FindVehicle", vehicle);
            }
            catch(Exception ex)
                {
                return Content(ex.Message);
            }
        }


     /*   [HttpGet]
        [Route("Edit/{id}")]
        public IActionResult Edit(int id)
        {

            return View("Edit", db.Vehicle.Find(id));
        }

        [HttpPost]
        [Route("Edit/{id?}")]
        public IActionResult Edit(Vehicle vehicle)
        {
     
            db.Entry(vehicle).State = EntityState.Modified;
            db.SaveChanges();


            return RedirectToAction("Index");
        }*/
    }
}