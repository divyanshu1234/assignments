﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrudCoreMvc.Models
{
    public class Vehicle
    {
        public int VehicleId { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
        public int Price { get; set; }
        public int Capacity { get; set; }
    }
}
