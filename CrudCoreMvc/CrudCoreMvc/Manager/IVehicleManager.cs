﻿using CrudCoreMvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrudCoreMvc.Manager
{
   public interface IVehicleManager
    {

        List<Vehicle> Display();
        Vehicle GetById(int vehicleId);
        Vehicle Create(Vehicle vehicle);
        Vehicle Delete(int vehicleId);
      
      
    }
}
