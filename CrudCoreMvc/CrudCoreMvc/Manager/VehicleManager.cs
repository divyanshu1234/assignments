﻿using CrudCoreMvc.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrudCoreMvc.Manager
{
    public class VehicleManager : IVehicleManager
    {
        public static List<Vehicle> listOfVehicle = new List<Vehicle>()
        {
            new Vehicle(){VehicleId=1,Name="bugat",Color="Red",Price=20,Capacity=220},
             new Vehicle(){VehicleId=2,Name="sdsds",Color="blue",Price=12,Capacity=224},
              new Vehicle(){VehicleId=3,Name="cczad",Color="grey",Price=34,Capacity=225},
               new Vehicle(){VehicleId=4,Name="njjsj",Color="purple",Price=43,Capacity=260},
        };
     //    private DataContext db = new DataContext();
       public List<Vehicle> Display()
        {
             if(listOfVehicle.Count()!=0)
            {
                return listOfVehicle;
            }
            return null;
        }

        public Vehicle GetById(int vehicleId)
        {
            Vehicle vehicle = listOfVehicle.FirstOrDefault(x => x.VehicleId == vehicleId);
                if(vehicle!=null)
            {
                return vehicle;
            }
            return null;
        }
        public Vehicle Create(Vehicle vehicle)
        { 
        

                listOfVehicle.Add(vehicle) ;
               
                
           
           
    return vehicle;
        }

   /*     public Vehicle Update(Vehicle vehicle)
        {
           

            if (vehicle != null)
            {
                db.Entry(vehicle).State = EntityState.Modified;
                db.SaveChanges();
             
            }
            return (vehicle);
        }*/

        public Vehicle Delete(int vehicleId)
        {
            Vehicle vehicle = listOfVehicle.FirstOrDefault(x => x.VehicleId == vehicleId);
          if(vehicle!=null)
            {
                listOfVehicle.Remove(vehicle);
            }
            return vehicle;

        }

    }
}
