using CrudCoreMvc.Manager;
using CrudCoreMvc.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace UnitTesting
{
    [TestClass]
    public class UnitTest1
    {
        VehicleManager vehicleTest = new VehicleManager();
        Vehicle vehicle = new Vehicle();
        [TestMethod]
        public void AddTest()
        {
            
         
            vehicle.VehicleId = 500;
            vehicle.Name = "abcd";
            vehicle.Color = "blue";
            vehicle.Price = 123;
            vehicle.Capacity = 1234;
            Vehicle actual = vehicleTest.Create(vehicle);
            Assert.AreEqual(vehicle, actual);
        }
        [TestMethod]
        public void DeleteTest()
        {
         
            vehicle.VehicleId = 4;
            Vehicle actual = vehicleTest.Delete(vehicle.VehicleId);
            Assert.AreEqual(actual.VehicleId, vehicle.VehicleId);
        }
        [TestMethod]
        public void FindTest()
        {
           
            vehicle.VehicleId = 3;
            Vehicle actual = vehicleTest.GetById(vehicle.VehicleId);
            Assert.AreEqual(actual.VehicleId, vehicle.VehicleId);
        }

    }
}
