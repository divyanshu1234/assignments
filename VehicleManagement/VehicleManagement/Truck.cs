﻿using System;
namespace VehicleManagement
{
    class Truck : Vehicle
    {
        public int Towing_capacity { get; set; }
        public void DisplayTruck()
        {
            Console.WriteLine(reg_no + "\t" + Name + "\t" + Color + "\t" + Towing_capacity);
        }
    }
}
