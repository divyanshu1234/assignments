﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace VehicleManagement
{
    class Program
    {
        static List<Car> listOfCar = new List<Car>();
        static List<Truck> listOfTruck = new List<Truck>();
        static int count = -1;
        static int isRegistered = 0;
        static void Main(string[] args)
        {
            do
            {
                Console.WriteLine("\nWelcome To the Vehicle Management System");
                Console.WriteLine("1.ADD A VEHICLE");
                Console.WriteLine("2.Delete A SPECIFIED VEHICLE");
                Console.WriteLine("3.Update A VEHICLE");
                Console.WriteLine("4.Find A VEHICLE");
                Console.WriteLine("5.List AVAILABLE VEHICLE'S");
                Console.WriteLine("6.Exit");
                Console.WriteLine("Enter your choice:");
                    int choice = int.Parse(Console.ReadLine());
                switch (choice)
                { 
                    case 1:
                        ++count;
                        AddVehicle(count);
                        
                        break;
                    case 2:
                        Console.WriteLine("\nEnter the registration number of vehicle you want to delete:");
                        int regist_no = int.Parse(Console.ReadLine());
                        DeleteVehicle(regist_no);
                        break;
                    case 3:
                        Console.WriteLine("\n Enter the registration number of the vehicle you want to Update:");
                        int regi_no = int.Parse(Console.ReadLine());
                        UpdateVehicle(regi_no);
                        break;
                    case 4:
                        Console.WriteLine("\nEnter the registration number of the Vehicle you want to Find:");
                        int registration_no = int.Parse(Console.ReadLine());
                        FindVehicle(registration_no);
                        break;
                    case 5:
                        Available(count);
                        break;
                    case 6:
                        Console.WriteLine("\nExit");
                        System.Environment.Exit(0);
                        break;
                }
            } while (true);
        }
        //adding vehicle
        static void AddVehicle(int n)
        {
                Console.WriteLine("Enter type of the Vehicle:\n 1. Car\n 2. Truck\n");
                int typeOfVehicle = int.Parse(Console.ReadLine());
                if (typeOfVehicle == 1)
                {
                var car = new Car();
                Console.WriteLine("Enter registration number of the Car");
                car.reg_no = int.Parse(Console.ReadLine());
                CheckRegistration(1, car.reg_no);
                if (isRegistered == 0)
                {
                    Console.WriteLine("the registration no. is already exist.");
                }
                else
                {
                    Console.WriteLine("Enter name of the Car");
                    car.Name = Console.ReadLine();
                    Console.WriteLine("Enter color of the Car");
                    car.Color = Console.ReadLine();
                    Console.WriteLine("Enter model of the Car");
                    car.Model = Console.ReadLine();
                    Console.WriteLine("Enter price of the Car");
                    car.price = int.Parse(Console.ReadLine());
                    listOfCar.Add(car);
                }
                }
                else if(typeOfVehicle == 2)
                {
                var truck = new Truck();
                Console.WriteLine("Enter registration number of the Truck");
                truck.reg_no = int.Parse(Console.ReadLine());
                CheckRegistration(2, truck.reg_no);
                if (isRegistered == 0)
                {
                    Console.WriteLine("the registration no. is already exist.");
                }
                else
                {
                    Console.WriteLine("Enter name of the Truck");
                    truck.Name = Console.ReadLine();
                    Console.WriteLine("Enter color of the Truck");
                    truck.Color = Console.ReadLine();
                    Console.WriteLine("Enter Towing Capacity of the Truck");
                    truck.Towing_capacity = int.Parse(Console.ReadLine());
                    listOfTruck.Add(truck);
                }
                }
                else
                {
                  Console.WriteLine("Wrong Choice Entered!!");
                }

                //
             /*   if(typeOfVehicle == Convert.ToInt32(VehicleEnum.Car))
               {
                Car car = new Car();
                //method
                //user input
                //car data
                car.AddVehicleDemo(car);
            }*/
        }
        //display vehicle
       static void Available(int n)
       {

                Console.WriteLine("Enter type of the Vehicle to display:\n 1. Car\n 2. Truck\n");
                int typeOfVehicle = int.Parse(Console.ReadLine());
                if (typeOfVehicle == 1)
                {
                  if (listOfCar != null && listOfCar.Count == 0)
                  {
                    Console.WriteLine("List is Empty!!");
                  }
                  else
                  {
                    Console.WriteLine("Car Details");
                    Console.WriteLine("---------------------------------------------------------------------------------------------------------------");
                    Console.WriteLine("id" + "\t" + "name" + "\t" + "color" + "\t" + "model" + "\t" + "price");
                    Console.WriteLine("----------------------------------------------------------------------------------------------------------------");
                    foreach (Car car in listOfCar)
                    {
                        car.DisplayCar();
                    }
                  }
                }
                else if(typeOfVehicle==2)
                {
                  if (listOfTruck != null && listOfTruck.Count == 0)
                  {
                    Console.WriteLine("List is Empty!!");
                  }
                  else
                  {
                    Console.WriteLine("Truck Details");
                    Console.WriteLine("---------------------------------------------------------------------------------------------------------------");
                    Console.WriteLine("id" + "\t" + "name" + "\t" + "color" + "\t" + "capacity");
                    Console.WriteLine("----------------------------------------------------------------------------------------------------------------");
                    foreach (Truck truck in listOfTruck)
                    {
                        truck.DisplayTruck();
                    }
                  }
                }
                else
                {
                    Console.WriteLine("Wrong Choice Entered!!");
                }
       }
        //deleting vehicle
       static void DeleteVehicle(int regist_no)
       {
                Console.WriteLine("Enter type of the Vehicle to delete\n 1. Car\n 2. Truck\n");
                int typeOfVehicle = int.Parse(Console.ReadLine());
                if (typeOfVehicle == 1)
                {
                     if (listOfCar != null && listOfCar.Count == 0)
                     {
                        Console.WriteLine("List is Empty!!");
                     }
                     else
                     {
                        int index = listOfCar.FindIndex(s => s.reg_no == regist_no);
                        listOfCar.RemoveAt(index);
                        Console.WriteLine("Successfully Deleted Details!!");
                     }
                }
                else if(typeOfVehicle == 2)
                {
                     if (listOfTruck != null && listOfTruck.Count == 0)
                     {
                        Console.WriteLine("List is Empty!!");
                     }
                    else
                    { 
                       int index = listOfTruck.FindIndex(s => s.reg_no == regist_no);
                       listOfTruck.RemoveAt(index);
                       Console.WriteLine("Successfully Deleted Details!!");
                    }
                } 
                else
                {
                      Console.WriteLine("Wrong Choice Entered!!");
                }
       }
        //Method to find Vehicle Index
       static void FindVehicle(int regi_no)
       {
            Console.WriteLine("Enter type of the Vehicle to Find\n 1. Car\n 2. Truck\n");
            int typeOfVehicle = int.Parse(Console.ReadLine());
            if (typeOfVehicle == 1)
            {
                if (listOfCar != null && listOfCar.Count == 0)
                {
                    Console.WriteLine("List is Empty!!");
                }
                else 
                {
                    int index = listOfCar.FindIndex(s=>s.reg_no==regi_no);
                    if (index == -1)
                    {
                        Console.WriteLine("Car Not Found!!!");
                    }
                    else
                    {
                        Console.WriteLine("Car Found at {0} index in car list", +index);
                    }
                }
            }
            else if(typeOfVehicle==2)
            {
                if (listOfTruck != null && listOfTruck.Count == 0)
                {
                    Console.WriteLine("List is Empty!!");
                }
                else
                {
                    int index = listOfTruck.FindIndex(s => s.reg_no == regi_no);
                    if (index == -1)
                    {
                        Console.WriteLine("Truck Not Found!!!");
                    }
                    else
                    {
                        Console.WriteLine("Truck Found at {0} index in truck list", +index);
                    }
                }
            }
            else
            {
                      Console.WriteLine("Wrong Choice Entered!!");
            }
       }
        //Method to update Vehicle's Details
        static void UpdateVehicle(int regi_no)
        {
            Console.WriteLine("Enter type of the Vehicle to update\n 1. Car\n 2. Truck\n");
            int typeOfVehicle = int.Parse(Console.ReadLine());
            if (typeOfVehicle == 1)
            {
                if (listOfCar != null && listOfCar.Count == 0)
                {
                    Console.WriteLine("List is Empty!!");
                }
                else
                {  
                   foreach(var listOfCar1 in listOfCar.ToList())
                    {
                        if (regi_no == listOfCar1.reg_no)
                        {
                            int index = listOfCar.FindIndex(s => s.reg_no == regi_no);
                            Console.WriteLine("Enter the option which you want to update/n");
                            Console.WriteLine("1.Name");
                            Console.WriteLine("2.Color");
                            Console.WriteLine("3.Model");
                            Console.WriteLine("4.price");
                            string option = Console.ReadLine();
                            switch(option)
                            {
                                case "1":
                                    Console.WriteLine("Enter Name");
                                    listOfCar[index].Name = Console.ReadLine();
                                    break;
                                case "2":
                                    Console.WriteLine("Enter color");
                                    listOfCar[index].Color = Console.ReadLine();
                                    break;
                                case "3":
                                    Console.WriteLine("Enter Model");
                                    listOfCar[index].Model = Console.ReadLine();
                                    break;
                                case "4":
                                    Console.WriteLine("Enter price");
                                    listOfCar[index].price =Convert.ToInt32( Console.ReadLine());
                                    break;
                                default:
                                    Console.WriteLine("Wrong choice entered");
                                    break;
                            }
                        }
                       
                    }
                    Console.WriteLine("car details updated successfully!!");
                }
            }
            else if(typeOfVehicle==2)
            {
                if (listOfTruck != null && listOfTruck.Count == 0)
                {
                    Console.WriteLine("List is Empty!!");
                }
                else
                {
                        foreach (var listOfTruck1 in listOfTruck.ToList())
                        {
                            if (regi_no == listOfTruck1.reg_no)
                            {
                                int index = listOfTruck.FindIndex(s => s.reg_no == regi_no);
                                Console.WriteLine("Enter the option which you want to update/n");
                                Console.WriteLine("1.Name");
                                Console.WriteLine("2.Color");
                                Console.WriteLine("3.Towing capacity");
                              
                                string option = Console.ReadLine();
                                switch (option)
                                {
                                    case "1":
                                        Console.WriteLine("Enter Name");
                                        listOfTruck[index].Name = Console.ReadLine();
                                        break;
                                    case "2":
                                        Console.WriteLine("Enter color");
                                        listOfTruck[index].Color = Console.ReadLine();
                                        break;
                                   
                                    case "3":
                                        Console.WriteLine("Enter Towing capacity");
                                        listOfTruck[index].Towing_capacity = Convert.ToInt32(Console.ReadLine());
                                        break;
                                    default:
                                        Console.WriteLine("Wrong choice entered");
                                        break;
                                }
                            }
                       

                        }
                    Console.WriteLine("truck details updated successfully!!");
                }
            }
            else
            {
                    Console.WriteLine("Wrong Choice Entered!!");
            }
        }
         static void CheckRegistration(int typeOfVehicle,int regis_no)
        {
            if (typeOfVehicle == 1)
            {
                if (listOfCar.Count() != 0)
                {
                    foreach (var mycar in listOfCar)
                    {
                        
                        if (regis_no ==mycar.reg_no )

                            isRegistered = 0;
                        else
                            isRegistered = 1;

                    }
                }
                else
                    isRegistered = 1;
            }
            else if (typeOfVehicle==2)
            {
                if (listOfTruck.Count() != 0)
                {
                    foreach (var mytrucklist1 in listOfTruck)
                    {
                        if (regis_no == mytrucklist1.reg_no)
                            isRegistered = 0;
                        else
                            isRegistered = 1;

                    }
                }
                else
                    isRegistered = 1;
            }
            else
            {
                Console.WriteLine("Wrong choice entered.");
            }
        }

    }
}
