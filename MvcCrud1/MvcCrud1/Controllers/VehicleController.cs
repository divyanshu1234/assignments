﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcCrud1.Models;

namespace MvcCrud1.Controllers
{
    public class VehicleController : Controller
    {
        // GET: Vehicle/Index
        public ActionResult Index()
        {
            using(DbModels dbModel=new DbModels())
            {
                return View(dbModel.Vehicles.ToList());
            }
            
        }

        // GET: Vehicle/Details/5
        public ActionResult Details(int id)
        {
            using(DbModels dbModel=new DbModels())
            {
                return View(dbModel.Vehicles.Where(x=>x.VehicleId==id).FirstOrDefault());
            }
           
        }

        // GET: Vehicle/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Vehicle/Create
        [HttpPost]
        public ActionResult Create(Vehicle vehicle)
        {
            try
            {
                using (DbModels dbModel = new DbModels())
                {
                    dbModel.Vehicles.Add(vehicle);
                    dbModel.SaveChanges();
                }
                    // TODO: Add insert logic here

                    return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Vehicle/Edit/5
        public ActionResult Edit(int id)
        {
            using (DbModels dbModel = new DbModels())
            {
                return View(dbModel.Vehicles.Where(x => x.VehicleId == id).FirstOrDefault());
            }
        }

        // POST: Vehicle/Edit/5
        [HttpPost]
        public ActionResult Edit(int id,Vehicle vehicle)
        {
            try
            {
                using(DbModels dbModel=new DbModels())
                {
                    dbModel.Entry(vehicle).State = EntityState.Modified;
                    dbModel.SaveChanges(); 
                }
                    // TODO: Add update logic here

                    return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Vehicle/Delete/5
        public ActionResult Delete(int id)
        {
            using (DbModels dbModel = new DbModels())
            {
                return View(dbModel.Vehicles.Where(x => x.VehicleId == id).FirstOrDefault());
            }
        }

        // POST: Vehicle/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                using(DbModels dbModel=new DbModels())
                {
                    Vehicle vehicle = dbModel.Vehicles.Where(x => x.VehicleId == id).FirstOrDefault();
                    dbModel.Vehicles.Remove(vehicle);
                    dbModel.SaveChanges();
                }
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
