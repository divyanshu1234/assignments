﻿namespace Calculator3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_Seven = new System.Windows.Forms.Button();
            this.button_Div = new System.Windows.Forms.Button();
            this.button_Nine = new System.Windows.Forms.Button();
            this.button_Eight = new System.Windows.Forms.Button();
            this.button_ClearEntry = new System.Windows.Forms.Button();
            this.button_Clear = new System.Windows.Forms.Button();
            this.button_Five = new System.Windows.Forms.Button();
            this.button_Six = new System.Windows.Forms.Button();
            this.button_Mul = new System.Windows.Forms.Button();
            this.button_Four = new System.Windows.Forms.Button();
            this.button_Two = new System.Windows.Forms.Button();
            this.button_Three = new System.Windows.Forms.Button();
            this.button_Sub = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button_Equal = new System.Windows.Forms.Button();
            this.button_Dot = new System.Windows.Forms.Button();
            this.button_Plus = new System.Windows.Forms.Button();
            this.button_Zero = new System.Windows.Forms.Button();
            this.textBox_Result = new System.Windows.Forms.TextBox();
            this.labelCurrentOperation = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button_Seven
            // 
            this.button_Seven.Location = new System.Drawing.Point(36, 67);
            this.button_Seven.Name = "button_Seven";
            this.button_Seven.Size = new System.Drawing.Size(45, 45);
            this.button_Seven.TabIndex = 0;
            this.button_Seven.Text = "7";
            this.button_Seven.UseVisualStyleBackColor = true;
            this.button_Seven.Click += new System.EventHandler(this.button_click);
            // 
            // button_Div
            // 
            this.button_Div.Location = new System.Drawing.Point(189, 67);
            this.button_Div.Name = "button_Div";
            this.button_Div.Size = new System.Drawing.Size(45, 45);
            this.button_Div.TabIndex = 1;
            this.button_Div.Text = "/";
            this.button_Div.UseVisualStyleBackColor = true;
            this.button_Div.Click += new System.EventHandler(this.operator_click);
            // 
            // button_Nine
            // 
            this.button_Nine.Location = new System.Drawing.Point(138, 67);
            this.button_Nine.Name = "button_Nine";
            this.button_Nine.Size = new System.Drawing.Size(45, 45);
            this.button_Nine.TabIndex = 2;
            this.button_Nine.Text = "9";
            this.button_Nine.UseVisualStyleBackColor = true;
            this.button_Nine.Click += new System.EventHandler(this.button_click);
            // 
            // button_Eight
            // 
            this.button_Eight.Location = new System.Drawing.Point(87, 67);
            this.button_Eight.Name = "button_Eight";
            this.button_Eight.Size = new System.Drawing.Size(45, 45);
            this.button_Eight.TabIndex = 3;
            this.button_Eight.Text = "8";
            this.button_Eight.UseVisualStyleBackColor = true;
            this.button_Eight.Click += new System.EventHandler(this.button_click);
            // 
            // button_ClearEntry
            // 
            this.button_ClearEntry.Location = new System.Drawing.Point(240, 67);
            this.button_ClearEntry.Name = "button_ClearEntry";
            this.button_ClearEntry.Size = new System.Drawing.Size(45, 45);
            this.button_ClearEntry.TabIndex = 4;
            this.button_ClearEntry.Text = "CE";
            this.button_ClearEntry.UseVisualStyleBackColor = true;
            this.button_ClearEntry.Click += new System.EventHandler(this.Button_ClearEntry_Click);
            // 
            // button_Clear
            // 
            this.button_Clear.Location = new System.Drawing.Point(240, 127);
            this.button_Clear.Name = "button_Clear";
            this.button_Clear.Size = new System.Drawing.Size(45, 45);
            this.button_Clear.TabIndex = 9;
            this.button_Clear.Text = "c";
            this.button_Clear.UseVisualStyleBackColor = true;
            this.button_Clear.Click += new System.EventHandler(this.Button_Clear_Click);
            // 
            // button_Five
            // 
            this.button_Five.Location = new System.Drawing.Point(87, 127);
            this.button_Five.Name = "button_Five";
            this.button_Five.Size = new System.Drawing.Size(45, 45);
            this.button_Five.TabIndex = 8;
            this.button_Five.Text = "5";
            this.button_Five.UseVisualStyleBackColor = true;
            this.button_Five.Click += new System.EventHandler(this.button_click);
            // 
            // button_Six
            // 
            this.button_Six.Location = new System.Drawing.Point(138, 127);
            this.button_Six.Name = "button_Six";
            this.button_Six.Size = new System.Drawing.Size(45, 45);
            this.button_Six.TabIndex = 7;
            this.button_Six.Text = "6";
            this.button_Six.UseVisualStyleBackColor = true;
            this.button_Six.Click += new System.EventHandler(this.button_click);
            // 
            // button_Mul
            // 
            this.button_Mul.Location = new System.Drawing.Point(189, 127);
            this.button_Mul.Name = "button_Mul";
            this.button_Mul.Size = new System.Drawing.Size(45, 45);
            this.button_Mul.TabIndex = 6;
            this.button_Mul.Text = "*";
            this.button_Mul.UseVisualStyleBackColor = true;
            this.button_Mul.Click += new System.EventHandler(this.operator_click);
            // 
            // button_Four
            // 
            this.button_Four.Location = new System.Drawing.Point(36, 127);
            this.button_Four.Name = "button_Four";
            this.button_Four.Size = new System.Drawing.Size(45, 45);
            this.button_Four.TabIndex = 5;
            this.button_Four.Text = "4";
            this.button_Four.UseVisualStyleBackColor = true;
            this.button_Four.Click += new System.EventHandler(this.button_click);
            // 
            // button_Two
            // 
            this.button_Two.Location = new System.Drawing.Point(87, 187);
            this.button_Two.Name = "button_Two";
            this.button_Two.Size = new System.Drawing.Size(45, 45);
            this.button_Two.TabIndex = 13;
            this.button_Two.Text = "2";
            this.button_Two.UseVisualStyleBackColor = true;
            this.button_Two.Click += new System.EventHandler(this.button_click);
            // 
            // button_Three
            // 
            this.button_Three.Location = new System.Drawing.Point(138, 187);
            this.button_Three.Name = "button_Three";
            this.button_Three.Size = new System.Drawing.Size(45, 45);
            this.button_Three.TabIndex = 12;
            this.button_Three.Text = "3";
            this.button_Three.UseVisualStyleBackColor = true;
            this.button_Three.Click += new System.EventHandler(this.button_click);
            // 
            // button_Sub
            // 
            this.button_Sub.Location = new System.Drawing.Point(189, 187);
            this.button_Sub.Name = "button_Sub";
            this.button_Sub.Size = new System.Drawing.Size(45, 45);
            this.button_Sub.TabIndex = 11;
            this.button_Sub.Text = "-";
            this.button_Sub.UseVisualStyleBackColor = true;
            this.button_Sub.Click += new System.EventHandler(this.operator_click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(36, 187);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(45, 45);
            this.button1.TabIndex = 10;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button_click);
            // 
            // button_Equal
            // 
            this.button_Equal.Location = new System.Drawing.Point(240, 187);
            this.button_Equal.Name = "button_Equal";
            this.button_Equal.Size = new System.Drawing.Size(45, 100);
            this.button_Equal.TabIndex = 19;
            this.button_Equal.Text = "=";
            this.button_Equal.UseVisualStyleBackColor = true;
            this.button_Equal.Click += new System.EventHandler(this.Button_Equal_Click);
            // 
            // button_Dot
            // 
            this.button_Dot.Location = new System.Drawing.Point(138, 242);
            this.button_Dot.Name = "button_Dot";
            this.button_Dot.Size = new System.Drawing.Size(45, 45);
            this.button_Dot.TabIndex = 17;
            this.button_Dot.Text = ".";
            this.button_Dot.UseVisualStyleBackColor = true;
            this.button_Dot.Click += new System.EventHandler(this.button_click);
            // 
            // button_Plus
            // 
            this.button_Plus.Location = new System.Drawing.Point(189, 242);
            this.button_Plus.Name = "button_Plus";
            this.button_Plus.Size = new System.Drawing.Size(45, 45);
            this.button_Plus.TabIndex = 16;
            this.button_Plus.Text = "+";
            this.button_Plus.UseVisualStyleBackColor = true;
            this.button_Plus.Click += new System.EventHandler(this.operator_click);
            // 
            // button_Zero
            // 
            this.button_Zero.Location = new System.Drawing.Point(36, 242);
            this.button_Zero.Name = "button_Zero";
            this.button_Zero.Size = new System.Drawing.Size(96, 45);
            this.button_Zero.TabIndex = 15;
            this.button_Zero.Text = "0";
            this.button_Zero.UseVisualStyleBackColor = true;
            this.button_Zero.Click += new System.EventHandler(this.button_click);
            // 
            // textBox_Result
            // 
            this.textBox_Result.Location = new System.Drawing.Point(36, 35);
            this.textBox_Result.Name = "textBox_Result";
            this.textBox_Result.Size = new System.Drawing.Size(249, 20);
            this.textBox_Result.TabIndex = 20;
            this.textBox_Result.Text = "0";
            this.textBox_Result.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBox_Result.TextChanged += new System.EventHandler(this.TextBox1_TextChanged);
            // 
            // labelCurrentOperation
            // 
            this.labelCurrentOperation.AutoSize = true;
            this.labelCurrentOperation.Location = new System.Drawing.Point(33, 19);
            this.labelCurrentOperation.Name = "labelCurrentOperation";
            this.labelCurrentOperation.Size = new System.Drawing.Size(0, 13);
            this.labelCurrentOperation.TabIndex = 21;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(301, 299);
            this.Controls.Add(this.labelCurrentOperation);
            this.Controls.Add(this.textBox_Result);
            this.Controls.Add(this.button_Equal);
            this.Controls.Add(this.button_Dot);
            this.Controls.Add(this.button_Plus);
            this.Controls.Add(this.button_Zero);
            this.Controls.Add(this.button_Two);
            this.Controls.Add(this.button_Three);
            this.Controls.Add(this.button_Sub);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button_Clear);
            this.Controls.Add(this.button_Five);
            this.Controls.Add(this.button_Six);
            this.Controls.Add(this.button_Mul);
            this.Controls.Add(this.button_Four);
            this.Controls.Add(this.button_ClearEntry);
            this.Controls.Add(this.button_Eight);
            this.Controls.Add(this.button_Nine);
            this.Controls.Add(this.button_Div);
            this.Controls.Add(this.button_Seven);
            this.Name = "Form1";
            this.Text = "Calculator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_Seven;
        private System.Windows.Forms.Button button_Div;
        private System.Windows.Forms.Button button_Nine;
        private System.Windows.Forms.Button button_Eight;
        private System.Windows.Forms.Button button_ClearEntry;
        private System.Windows.Forms.Button button_Clear;
        private System.Windows.Forms.Button button_Five;
        private System.Windows.Forms.Button button_Six;
        private System.Windows.Forms.Button button_Mul;
        private System.Windows.Forms.Button button_Four;
        private System.Windows.Forms.Button button_Two;
        private System.Windows.Forms.Button button_Three;
        private System.Windows.Forms.Button button_Sub;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button_Equal;
        private System.Windows.Forms.Button button_Dot;
        private System.Windows.Forms.Button button_Plus;
        private System.Windows.Forms.Button button_Zero;
        private System.Windows.Forms.TextBox textBox_Result;
        private System.Windows.Forms.Label labelCurrentOperation;
    }
}

