﻿using CodeFirstAssignment.DAL;
using CodeFirstAssignment.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Data.Entity;
using System.Linq;

namespace CodeFirstAssignment.Manager
{
    class VehicleManager : IVehicleManager
    {
        private VehicleContext db = new VehicleContext();
        private DbSet<Vehicle> dbSet;
        public VehicleManager()
        {
             dbSet = db.Set<Vehicle>();
        }

        /// <summary>
        /// Creating new vehicle
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public int Create(Vehicle vehicle)
        {
            if (vehicle != null)
            {
                string fileName = Path.GetFileNameWithoutExtension(vehicle.ImageFile.FileName);
                string extension = Path.GetExtension(vehicle.ImageFile.FileName);
                fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                vehicle.ImagePath = "~/Image/" + fileName;
                fileName = Path.Combine(HttpContext.Current.Server.MapPath("~/Image/"), fileName);
                vehicle.ImageFile.SaveAs(fileName);
                db.Vehicles.Add(vehicle);
                int result = db.SaveChanges();
                return result;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Get vehicle by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Vehicle GetById(object id)
        {
            return dbSet.Find(id);
        }

        /// <summary>
        /// To Get all Vehicles
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Vehicle> GetAll()
        {
            return dbSet.ToList();
        }

        /// <summary>
        /// Editing the vehicle details
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public int Edit(Vehicle vehicle)
        {
            if (vehicle != null)
            {
                db.Entry(vehicle).State = EntityState.Modified;
                int result = db.SaveChanges();
                return result;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Deleting the Vehicle
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int Delete(int id)
        {
            Vehicle vehicle = db.Vehicles.Find(id);
            db.Vehicles.Remove(vehicle);
            int result = db.SaveChanges();
            return result;
        }
     
    }
}