﻿using CodeFirstAssignment.Models;
using System.Collections.Generic;

namespace CodeFirstAssignment.Manager
{
  public  interface IVehicleManager
    {
        IEnumerable<Vehicle> GetAll();
        Vehicle GetById(object id);
        int Create(Vehicle vehicle);
        int Edit(Vehicle vehicle);
        int Delete(int id);
    }
}
