﻿using System;
using System.Data;
using System.Net;
using System.Web.Mvc;
using CodeFirstAssignment.DAL;
using CodeFirstAssignment.Manager;
using CodeFirstAssignment.Models;

namespace CodeFirstAssignment.Controllers
{
    public class VehiclesController : Controller
    {
        private VehicleContext db = new VehicleContext();
        IVehicleManager iVehicle;
        public VehiclesController(IVehicleManager ivehicle)
        {
            iVehicle = ivehicle;
        }

        /// <summary>
        /// display the index
        /// </summary>
        /// <returns></returns>
        // GET: Vehicles
        public ActionResult Index()
        {
            var vehicles = iVehicle.GetAll();
            return View(vehicles);
        }

        /// <summary>
        /// Details Of vehicle
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: Vehicles/Details/1
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Vehicle vehicle = iVehicle.GetById(id);
            if (vehicle == null)
            {
                return HttpNotFound();
            }
            return View(vehicle);
        }

        /// <summary>
        /// Creating new vehicle get request
        /// </summary>
        /// <returns></returns>
        // GET: Vehicles/Create
        public ActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// creating new Vehicle post request
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Vehicle vehicle)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    iVehicle.Create(vehicle);
                    return RedirectToAction("Index");
                }
            }
            catch(Exception)
            {
                ModelState.AddModelError(string.Empty, "Unable to save changes. Try again, and if the problem persists contact your system administrator.");
            }
            return View(vehicle);
        }

        /// <summary>
        /// Editing the vehicle get request
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: Vehicles/Edit/1
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehicle vehicle = iVehicle.GetById(id);
            if (vehicle == null)
            {
                return HttpNotFound();
            }
            return View(vehicle);
        }

      /// <summary>
      /// Editing the vehicle post request
      /// </summary>
      /// <param name="vehicle"></param>
      /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Vehicle vehicle)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    iVehicle.Edit(vehicle);
                    return RedirectToAction("Index");
                }
            }
            catch(Exception)
            {
                ModelState.AddModelError(string.Empty, "Unable to save changes. Try again, and if the problem persists contact your system administrator.");
            }
            return View(vehicle);
        }

        /// <summary>
        /// Searching the Vehicle
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public ActionResult Search(Vehicle vehicle)
        {
            var vehicles=db.Vehicles;
            foreach (Vehicle item in vehicles)
            {
                if(item.VehicleId==vehicle.VehicleId)
                {
                    return View("Edit", item);
                }
               }
            ViewBag.result = "does not exist";
            return View("Edit");
        }

        /// <summary>
        /// Deleting the Vehicle Get Request
        /// </summary>
        /// <param name="id"></param>
        /// <param name="saveChangesError"></param>
        /// <returns></returns>
        // GET: Vehicles/Delete/1
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehicle vehicle = iVehicle.GetById(id);
            if (vehicle == null)
            {
                return HttpNotFound();
            }
            return View(vehicle);
        }

        /// <summary>
        /// Deleting the vehicle post request
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // POST: Vehicles/Delete/1
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                iVehicle.Delete(id);
            }
            catch(Exception)
            {
                return RedirectToAction("Delete");
            }
            return RedirectToAction("Index");
        }
    
    }
}
