﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace CodeFirstAssignment.Models
{
    public class Vehicle
    {
        public int VehicleId { get; set; }
        [Required(ErrorMessage = "Please enter name"), MaxLength(30)]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please enter color"), MaxLength(30)]
        public string Color { get; set; }
        [Required(ErrorMessage = "Please enter price")]
        public int Price { get; set; }

        [DisplayName("Upload File")]
        public string ImagePath { get; set; }

        [NotMapped]
        public HttpPostedFileBase ImageFile { get; set; }
    }
}