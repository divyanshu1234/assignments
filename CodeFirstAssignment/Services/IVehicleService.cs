﻿using CodeFirstAssignment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public interface IVehicleService
    {
        
            IEnumerable<Vehicle> GetAll();
            Vehicle GetById(object id);
            int CreateVehicle(Vehicle vehicle);
            int EditVehicle(Vehicle vehicle);
            int DeleteVehicle(int id);
        
    }
}
