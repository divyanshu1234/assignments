﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class WepApiConsumeController : Controller
    {
        // GET: WepApiConsume
        public ActionResult Index()
        {
            IEnumerable<Vehicle> vehicles = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44333/api/");
                //HTTP GET
               var responseTask = client.GetAsync("Vehicle");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<Vehicle>>();
                    readTask.Wait();

                    vehicles = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    vehicles = Enumerable.Empty<Vehicle>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(vehicles);
        }
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Vehicle vehicle)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44333/api/Vehicle");

                //HTTP POST
                var postTask = client.PostAsJsonAsync<Vehicle>("vehicle", vehicle);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");

            return View(vehicle);
        }
        public ActionResult Edit(int id)
        {
            Vehicle vehicle = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44333/api/");
                //HTTP GET
                var responseTask = client.GetAsync("vehicle?id=" + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Vehicle>();
                    readTask.Wait();

                    vehicle = readTask.Result;
                }
            }

            return View(vehicle);
        }
        [HttpPost]
        public ActionResult Edit(Vehicle vehicle)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44333/api/Vehicle");

                //HTTP POST
                var putTask = client.PutAsJsonAsync<Vehicle>("vehicle", vehicle);
                putTask.Wait();

                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    return RedirectToAction("Index");
                }
            }
            return View(vehicle);
        }
        public ActionResult Delete(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44333/api");

                //HTTP DELETE
                var deleteTask = client.DeleteAsync("vehicle/" + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    return RedirectToAction("Index");
                }
            }

            return RedirectToAction("Index");
        }
    }

}



