﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Http;
using WebApi.Manager;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class VehicleController : ApiController
    {
        private DBModel db = new DBModel();
        private   IVehicleManager vehicleManager;

        public VehicleController(IVehicleManager ivehicle) 
        {
            this.vehicleManager = ivehicle;
        }

        /// <summary>
        /// for get vehicle details
        /// </summary>
        /// <returns></returns>
        // GET: api/Vehicle
        [HttpGet]
        public IQueryable<Vehicle> Get()
        {
            var vehicles = vehicleManager.GetAll();
            return vehicles;
        }

        /// <summary>
        /// for get vehicle by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Vehicle/5
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            Vehicle vehicle = vehicleManager.GetById(id);
            if (vehicle == null)
            {
                return NotFound();
            }
            return Ok(vehicle);
        }

        /// <summary>
        /// for updating the vehicle details
        /// </summary>
        /// <param name="id"></param>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        // PUT: api/Vehicle/5
        [HttpPut]
        public IHttpActionResult Put(int id, Vehicle vehicle)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != vehicle.VehicleId)
            {
                return BadRequest();
            }

            db.Entry(vehicle).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (Exception)
            {
                if (!VehicleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// For creating new vehicle
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        // POST: api/Vehicle
        [HttpPost]
        public IHttpActionResult Post(Vehicle vehicle)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            int result = vehicleManager.Create(vehicle);

            if (result > 0)
            {
                return Ok(vehicle);
            }
            else
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// for deleting the vehicle
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/Vehicle/1
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            Vehicle vehicle = vehicleManager.GetById(id);
            if (vehicle == null)
            {
                return NotFound();
            }
            vehicleManager.Delete(id);
            return Ok(vehicle);
        }

        /// <summary>
        /// Searching the existing vehicle
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("api/Vehicle/VehicleExists/{id:int}")]
        private bool VehicleExists(int id)
        {
            return vehicleManager.Existsence(id);
        }
    }
}