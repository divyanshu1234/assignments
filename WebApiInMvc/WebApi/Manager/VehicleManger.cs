﻿using System.Data.Entity;
using System.Linq;
using WebApi.Models;

namespace WebApi.Manager
{
    public class VehicleManager:IVehicleManager
    {
        private DBModel db = new DBModel();

        /// <summary>
        /// for getting all vehicles
        /// </summary>
        /// <returns></returns>
        public IQueryable<Vehicle> GetAll()
        {
            return db.Vehicles;
        }

        /// <summary>
        /// for getting vehicle by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Vehicle GetById(object id)
        {
            Vehicle vehicle = db.Vehicles.Find(id);
            return vehicle;
        }

        /// <summary>
        /// for creating vehicle
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public int Create(Vehicle vehicle)
        {
            db.Vehicles.Add(vehicle);
            int result=db.SaveChanges();
            return result;
        }

        /// <summary>
        /// for updating vehicle
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public int Update(Vehicle vehicle)
        {
            db.Entry(vehicle).State = EntityState.Modified;
            int result=db.SaveChanges();
            return result;
        }

        /// <summary>
        /// for deleting vehicle
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int Delete(int id)
        {
            Vehicle vehicle = db.Vehicles.Find(id);
            db.Vehicles.Remove(vehicle);
            int result=  db.SaveChanges();
            return result;
        }
        public bool Existsence(int id)
        {
            bool result= db.Vehicles.Count(e => e.VehicleId == id) > 0;
            return result;
        }
    }
}