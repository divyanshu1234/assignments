﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Models;

namespace WebApi.Manager
{
    public interface IVehicleManager
    {
        IQueryable<Vehicle> GetAll();
        Vehicle GetById(object id);
        int Create(Vehicle vehicle);
        int Update(Vehicle vehicle);
        int Delete(int id);
        bool Existsence(int id);

    }
    
}