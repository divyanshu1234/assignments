
namespace WebApi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Vehicle
    {
        public int VehicleId { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
        public Nullable<int> Price { get; set; }
        public Nullable<int> Capacity { get; set; }
    }
}
